<?php

// namespace App;
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Shared_leads extends Model
{   
    
     public function user()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function place()
    {
        return $this->hasOne(Place::class, 'id', 'place_id');
    }

     public function booking()
    {
        return $this->hasOne(Booking::class, 'id', 'bookking_id');
    }
}
