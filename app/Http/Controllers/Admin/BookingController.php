<?php

namespace App\Http\Controllers\Admin;


use App\Commons\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;
use App\Models\Booking;
use App\Models\Shared_leads;
use App\Models\Amenities;
use App\Models\Category;
use App\Models\City;
use Astrotomic\Translatable\Validation\RuleFactory;
use App\Models\Country;
use App\Models\Place;
use App\Models\PlaceType;
use App\Models\Review;
use Log;
use Carbon\Carbon;
use Illuminate\Http\Request;use Redirect;
use Illuminate\Support\Facades\Auth;

class BookingController extends Controller
{   
    private $place;
    private $country;
    private $city;
    private $category;
    private $amenities;
    private $response;

    public function __construct(Place $place, Country $country, City $city, Category $category, Amenities $amenities, Response $response){
        $this->place = $place;
        $this->country = $country;
        $this->city = $city;
        $this->category = $category;
        $this->amenities = $amenities;
        $this->response = $response;
    }
    public function list()
    {
        $bookings = Booking::query()
            ->with('user')
            ->with('place')
            ->orderBy('created_at', 'desc')
            ->get();

//        return $bookings;

        return view('admin.booking.booking_list', [
            'bookings' => $bookings
        ]);
    }

    public function updateStatus(Request $request)
    {   
        // return $request->all();
        $data = $this->validate($request, [
            'status' => 'required',
        ]);

        $model = Booking::find($request->booking_id);
        $model->fill($data)->save();

        // Mail::send('frontend.mail.new_booking', [
        //         'name' => $request->booking_name,
        //         'email' => $request->booking_email,
        //         'phone' => $request->phone_number,
        //         'place' => $request->booking_place_name,
        //         'datetime' => $request->booking_time_date,
        //         'numberofadult' => $request->numbber_of_adult,
        //         'numberofchildren' => $request->numbber_of_children,
        //         'text_message' => $request->booking_message,
        //         'booking_at' => $request->booking_at
        //     ], function ($message) use ($request) {
        //         $message->to(setting('email_system'), "{$request->booking_name}")->subject('Booking from ' . $request->booking_name);
        //     });

        return back()->with('success', 'Update status success!');
    }

    public function shareLeads(Request $request){
        // $place = $this->place->getBySlug($request->slug);
        if ($request->booking_place_slug) {
            $place = $this->place->getBySlug($request->booking_place_slug);
        }else{
            // return $request->place_id;
            $place = Place::where('id', $request->place_id)->first();
            // return $place;
        }
        if($request->id){
            $leads = Shared_leads::find($request->id);
        }
        // $booking = Booking::find($request->id);
        $booking = Booking::find($request->booking_id);
        if (!$place) abort(404);
        $city = City::query()
            ->with('country')
            ->where('id', $place->city_id)
            ->first();

        $amenities = Amenities::query()
            ->whereIn('id', $place->amenities ? $place->amenities : [])
            ->get(['id', 'name', 'icon']);

        $categories = Category::query()
            ->whereIn('id', $place->category ? $place->category : [])
            ->get(['id', 'name', 'slug', 'icon_map_marker']);

        $place_types = PlaceType::query()
            ->whereIn('id', $place->place_type ? $place->place_type : [])
            ->get(['id', 'name']);

        $reviews = Review::query()
            ->with('user')
            ->where('place_id', $place->id)
            ->where('status', Review::STATUS_ACTIVE)
            ->get();
        $review_score_avg = Review::query()
            ->where('place_id', $place->id)
            ->where('status', Review::STATUS_ACTIVE)
            ->avg('score');

        $similar_places = Place::query()
            ->with('place_types')
            ->with('avgReview')
            ->withCount('reviews')
            ->withCount('wishList')
            ->where('city_id', $city->id)
            ->where('id', '<>', $place->id);
        foreach ($place->place_type as $place_type):
            $similar_places->where('place_type', 'like', "%{$place_type}%");
        endforeach;
        $similar_places = $similar_places->get();;
        // return $categories;

        return view('admin.booking.share_leads',[
            'place' => $place,
            'city' => $city,
            'amenities' => $amenities,
            'categories' => $categories,
            'place_types' => $place_types,
            'reviews' => $reviews,
            'review_score_avg' => $review_score_avg,
            'similar_places' => $similar_places,
            'booking' => $booking,
            'leads' => $leads
        ]);
    }

    public function booking(Request $request)
    {
        $request['user_id'] = Auth::id();
        if ($request->date) {
            $request['date'] = Carbon::parse($request->date);
        }

        $booking = new Shared_leads;
        // $rule_factory = RuleFactory::make( [
        //     'user_id' => '',
        //     'place_id' => '',
        // ]);
        // $data = $this->validate($request, $rule_factory);
        // $booking->fill($data);
        // return var_dump(Shared_leads::select('place_name')->first());
        // return;
        $booking->save();
        $booking->user_id = $request['user_id'];
        $booking->place_id = serialize($request->place_id);
        $booking->place_name = serialize($request->place_name);
        $booking->place_email = serialize($request->place_email);
        $booking->booking_id = $request->booking_id;
        $booking->numbber_of_adult = $request->user_no_of_adults;
        $booking->numbber_of_childern = $request->user_no_of_childers;
        $booking->date = formatDate($request->user_booking_at, 'Y-m-d');
        $booking->time = formatDate($request->user_booking_at, 'H:i:s');

        $max= count($request->place_name)-1;
        $booking->save();
        $data = 'check';
        if ($data) {
            // $place = Place::find($request['place_id']);

            if ($request->type == Booking::TYPE_CONTACT_FORM) {
                Log::debug("Booking::TYPE_CONTACT_FORM: " . $request->user_booking_type);
                $name = $request->user_name;
                $email = $request->user_email;
                $phone = $request->user_phone_number;
                $datetime = $request->user_booking_at;
                $numberofadult = $request->user_no_of_adults;
                $numberofchildren = $request->user_no_of_childers;
                $text_message = $request->user_message;
            } else {
                Log::debug("Booking::submit: " . $request->user_booking_type);
                $name = user()->name;
                $email = user()->email;
                $phone = user()->phone_number;
                $datetime = Carbon::parse($booking->date)->format('Y-m-d') . " " . $booking->time;
                $numberofadult = $request->user_no_of_adults;
                $numberofchildren = $request->user_no_of_childers;
                $text_message = $request->user_message;
            }

        for($x=0;$x<=$max;$x++){
            $place = $request->place_name[$x];
            $emails = $request->place_email[$x];
            Mail::send('admin.mail.new_booking', [
                'name' => $name,
                'email' => $email,
                'phone' => $phone,
                'place' => $place,
                'datetime' => $datetime,
                'numberofadult' => $numberofadult,
                'numberofchildren' => $numberofchildren,
                'text_message' => $text_message,
                'booking_at' => $request->booking_at,
            ], function ($message) use ($request,$emails,$place) {
                $message->to($emails,$place)->subject('Booking from' . $request->user_name);
            });
        }

        }
        Booking::where('id',$request->booking_id)->update(['status'=>3]);
        $request->session()->flash('success','Successfully shared leads....');
        return redirect(route('admin_booking_list'));//$this->response->formatResponse(200, $booking, 'You successfully created your booking!');
    }

    public function checkLeads(Request $request){
        $place_id = $request->place_id;
        $place_name = $request->place_name;
        $booking_id = $request->booking_id;
        $count = count($place_id)-1;
        // $data = new Shared_leads;
        for($i=0;$i <= $count ;$i++ ){
            $str = serialize($place_id[$i]);
            $data = Shared_leads::where('booking_id',$booking_id)->distinct('place_name')->where('place_id','like','%'.$str.'%');
        }
        $data = $data->get();
        $place_name =[];
        foreach($data as $name){
                    array_push($place_name,unserialize($name->place_name));
                }
        if(count($data)>0){
            return $place_name;
        }else{
            return count($data);
        }
        // $place_id = array();
        // $place_id = $request->place_id;
        // // $place_id = is_array($place_id) ? $place_id : [$place_id];
        // $places = count($place_id)-1;
        // // return var_dump($place_id);
        // $place_name = array();
        // $booking_id = $request->booking_id;
        //     return $places;

        // $data = Shared_leads::query();
        //     // ->select('place_name')
        //     // return $data;
        //         // $data = Shared_leads::where('booking_id', $booking_id)
        // for($i=0;$i <= $places ; $i++){
        //     $str = $place_id[$i];
        //     $sr = serialize($str);
        //     $data->where('booking_id', $booking_id)
        //     // ->where('place_name',$request->place_name)
        //     ->where('place_id','like','%'.$sr.'%');
        //     }
        //     // $data->get();
        //         $data = $data->get();
        //         return $data;
        //         foreach($data as $name){
        //             array_push($place_name,unserialize($name->place_name));
        //         }
        // if(count($data)>0){
        //     // $place_name = count($data);
        //     return $place_name;
        // }else{
        //     return 'share';
        // }
        
        // return;
        // Shared_leads::where()->get();
    }

    public function shareLeadsList(){
         $leads = Shared_leads::query()
            ->with('user')
            ->with('place')
            ->orderBy('created_at', 'desc')
            ->get();

       // return $leads;

        return view('admin.booking.share_leads_list', [
            'leads' => $leads
        ]);
        // $data['leads'] = ShareLeads::all()
        // return view('admin.booking.shared_leads_list',$data);
    }
}