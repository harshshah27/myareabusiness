@extends('frontend.layouts.template_02')
@section('main')
	
                        @foreach($places as $place_type)
                            <tr>
                                <td>{{$place_type->id}}</td>
                                <td>{{$place_type->name}}</td>
                                <!-- <td>{{$place_type['category']['name']}}</td> -->
                            </tr>
                        @endforeach
                        <section class="above-fold sub-category-wrapper">
                        	<div class="container box-wrap">
                        		<div class="page-heading align-center">
									<h1 class="title-primary">Best Home Appliances Repair &amp; Services in Chennai</h1>
										<span class="text-white">Professional Technicians | Best Price Guaranteed | Doorstep Service</span>
								</div>
								<div class="clearfix">
									<div class="sub-category-holder">
										<div class="subcategory-step icon-view">
											<div class="title-large light">What service do you required?</div>
											<div class="icon-wrap">
												<div class="icon-group row">
													<div class="icon col-md-2">
														<div class="group">
															<img src="{{getImageUrl('air-conditioner.png')}}">
															<h5>TV Repair</h5>
														</div>
													</div>
													<div class="icon col-md-2">
														<div class="group">
															<img src="{{getImageUrl('air-conditioner.png')}}">
															<h5>TV Repair</h5>
														</div>
													</div>
													<div class="icon col-md-2">
														<div class="group">
															<img src="{{getImageUrl('air-conditioner.png')}}">
															<h5>TV Repair</h5>
														</div>
													</div>
													<div class="icon col-md-2">
														<div class="group">
															<img src="{{getImageUrl('air-conditioner.png')}}">
															<h5>TV Repair</h5>
														</div>
													</div>

													<div class="icon col-md-2">
														<div class="group">
															<img src="{{getImageUrl('air-conditioner.png')}}">
															<h5>TV Repair</h5>
														</div>
													</div>
													<div class="icon col-md-2">
														<div class="group">
															<img src="{{getImageUrl('air-conditioner.png')}}">
															<h5>TV Repair</h5>
														</div>
													</div>
													<div class="icon col-md-2">
														<div class="group">
															<img src="{{getImageUrl('air-conditioner.png')}}">
															<h5>TV Repair</h5>
														</div>
													</div>
													<div class="icon col-md-2">
														<div class="group">
															<img src="{{getImageUrl('air-conditioner.png')}}">
															<h5>TV Repair</h5>
														</div>
													</div>
													<div class="icon col-md-2">
														<div class="group">
															<img src="{{getImageUrl('air-conditioner.png')}}">
															<h5>TV Repair</h5>
														</div>
													</div>
													<div class="icon col-md-2">
														<div class="group">
															<img src="{{getImageUrl('air-conditioner.png')}}">
															<h5>TV Repair</h5>
														</div>
													</div>
													<div class="icon col-md-2">
														<div class="group">
															<img src="{{getImageUrl('air-conditioner.png')}}">
															<h5>TV Repair</h5>
														</div>
													</div>
													<div class="icon col-md-2">
														<div class="group">
															<img src="{{getImageUrl('air-conditioner.png')}}">
															<h5>TV Repair</h5>
														</div>
													</div>

													<div class="icon col-md-2">
														<div class="group">
															<img src="{{getImageUrl('air-conditioner.png')}}">
															<h5>TV Repair</h5>
														</div>
													</div>
													<div class="icon col-md-2">
														<div class="group">
															<img src="{{getImageUrl('air-conditioner.png')}}">
															<h5>TV Repair</h5>
														</div>
													</div>
													
												</div>

											</div>
										</div>
									</div>
								</div>
                        	</div>
                        </section>

                        <section class="" style="padding-top: 70px;padding-bottom: 50px">
                        	<div class="text-center container">
                        		<div class="page-heading">
        							<h1 class="text-black">Welcome to My Area Business</h2>
        								<p>
        									Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
        								</p>
								</div>
								
                        	</div>
                        	
                        </section>
							
                        <section class="listing-section">
                        	<div class="container">
                        		<div class="page-heading">
                        			<h2 class="title-large">Top Latest Home Appliances Listing</h2>
                        		</div>
                        		<div class="listing-card card p-3">
                        			<div class="row col-md-12">
                        				<div class="col-md-10 p-0 text-right">
                        					<div class="initial-letter random-1">S</div>
                        					<div class="listing-detail">
                        						<div class="name">
                        							<a class="title-xlarge">Sri Anu Furniture</a>
                        							<div class="verified-tag">Verified</div>
                        						</div>
                        						<div class="data">
                        							<span class="mobile"><i class="las la-phone mr-2"></i> 080-43691157</span>
                        							<span>Kukatpally, Hyderabad, 500072</span>
                        							<a class="view-more" href="#" target="_blank">Get Direction</a>
                        						</div>	
                        					</div>
                        				</div>
                        				<div class="col-md-2 p-0 text-right">
                        					<div class="ratings-group">
                        						<div class="ratings medium">
                        							<span>
                        							<i class="las la-star"></i>
                        							<i class="las la-star"></i>
                        							<i class="las la-star"></i>
                        							<i class="las la-star"></i>
                        							<i class="las la-star"></i>
                        							</span>
                        						</div>
                        						<b>3.5/5</b>
                        						<p class="font-weight-bold">Based on 14 reviews</span>
                        					</div>
                        					
                        				</div>
                        			</div>
                        			<div class="row col-md-12">
                        					<div class="tags">
												<label class="mr-2 font-weight-bold">Services offered:</label>
												<span>Furniture Dealers, Mattress &amp; Beddings, Purchase new furniture</span>
											</div>	
                        			</div>
                        			<div class="row col-md-12">
                        				<div class="col-md-10 p-0">
                        					<div class="key-points">
												<span>Sulekha score<b>7.7</b></span>
												<span>Years of experience<b>24 years </b></span>
												<span>No. of times hired<b>2 times </b></span>
												<span>Working hours<b>10.35 AM to 08.35 PM </b></span>
											</div>
                        				</div>
                        				<div class="col-md-2 p-0 text-right">
                        					<button class="btn btn-primary">GET QUOTE</button>
                        				</div>
                        			</div>

                        			<div class="row col-md-12">
                        				
                        					<div class="recent-reviews">
                        					<div class="name"><b>Recent review</b>Avinash from Hyderabad</div>
                        					<div class="review">
													<span>
                        							<i class="las la-star"></i>
                        							<i class="las la-star"></i>
                        							<i class="las la-star"></i>
                        							<i class="las la-star"></i>
                        							<i class="las la-star"></i>
                        							</span>
												<p style="font-size: 16px">"Lovely ambience, cordial staff."</p>
											</div>
                        				</div>
                        				
                        				
                        			</div>
                        		</div>

                        		<div class="listing-card card p-3">
                        			<div class="row col-md-12">
                        				<div class="col-md-10 p-0 text-right">
                        					<div class="initial-letter random-1">S</div>
                        					<div class="listing-detail">
                        						<div class="name">
                        							<a class="title-xlarge">Sri Anu Furniture</a>
                        							<div class="verified-tag">Verified</div>
                        						</div>
                        						<div class="data">
                        							<span class="mobile"><i class="las la-phone mr-2"></i> 080-43691157</span>
                        							<span>Kukatpally, Hyderabad, 500072</span>
                        							<a class="view-more" href="#" target="_blank">Get Direction</a>
                        						</div>	
                        					</div>
                        				</div>
                        				<div class="col-md-2 p-0 text-right">
                        					<div class="ratings-group">
                        						<div class="ratings medium">
                        							<span>
                        							<i class="las la-star"></i>
                        							<i class="las la-star"></i>
                        							<i class="las la-star"></i>
                        							<i class="las la-star"></i>
                        							<i class="las la-star"></i>
                        							</span>
                        						</div>
                        						<b>3.5/5</b>
                        						<p class="font-weight-bold">Based on 14 reviews</span>
                        					</div>
                        					
                        				</div>
                        			</div>
                        			<div class="row col-md-12">
                        					<div class="tags">
												<label class="mr-2 font-weight-bold">Services offered:</label>
												<span>Furniture Dealers, Mattress &amp; Beddings, Purchase new furniture</span>
											</div>	
                        			</div>
                        			<div class="row col-md-12">
                        				<div class="col-md-10 p-0">
                        					<div class="key-points">
												<span>Sulekha score<b>7.7</b></span>
												<span>Years of experience<b>24 years </b></span>
												<span>No. of times hired<b>2 times </b></span>
												<span>Working hours<b>10.35 AM to 08.35 PM </b></span>
											</div>
                        				</div> @guest
                        				<div class="col-md-2 p-0 text-right">
                        					<a class="open-login btn btn-primary" href="#" data-toggle="modal" data-target="#getQuoteForm" >GET QUOTE</a>
                        				</div>@endguest
                        			</div>

                        			<div class="row col-md-12">
                        				
                        					<div class="recent-reviews">
                        					<div class="name"><b>Recent review</b>Avinash from Hyderabad</div>
                        					<div class="review">
													<span>
                        							<i class="las la-star"></i>
                        							<i class="las la-star"></i>
                        							<i class="las la-star"></i>
                        							<i class="las la-star"></i>
                        							<i class="las la-star"></i>
                        							</span>
												<p style="font-size: 16px">"Lovely ambience, cordial staff."</p>
											</div>
                        				</div>
                        				
                        				
                        			</div>
                        		</div>
                        		

                        	</div>
                        </section>

@stop