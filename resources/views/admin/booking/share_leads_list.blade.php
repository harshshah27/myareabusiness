@extends('admin.layouts.template')
@section('main')
    <div class="page-title">
        <div class="title_left">
            <h3>Leads</h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_content">

                    <table class="table table-striped table-bordered golo-datatable">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Place</th>
                            <th>Shared at</th>
                            <th class="action">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                            @php $bookings = $leads @endphp
                        @foreach($bookings as $booking)
                            <tr>
                                <td>{{$booking->id}}</td>
                                    <td>{{$booking['user']['name']}}
                                        <small>(UID: {{$booking['user']['id']}})</small>
                                    </td>

                                @if(isset($booking['place']))
                                    <td><a href="{{route('place_detail', $booking['place']['slug'])}}" target="_blank">{{$booking['place']['name']}}</a></td>
                                @else
                                    <td>
                                        @php $places_id = unserialize($booking->place_id) @endphp
                                        <i>Place ID : </i>
                                        @foreach($places_id as $place)
                                        {{$place}},
                                        @endforeach
                                        @php $places = unserialize($booking->place_name) @endphp
                                        <i>&emsp;Name : </i>
                                        @foreach($places as $place)
                                            <p id='places' class='d-none places' data-place='{{$place}}'>{{$place}}&emsp;</p>{{$place}},
                                            <!-- <p></p> -->
                                        @endforeach
                                    </td>
                                @endif
                                <td>{{formatDate($booking->created_at, 'H:i d/m/Y')}}</td>
                                <td>
                                    <button class="btn btn-primary shared_detail"
                                                data-id="{{$booking->id}}"
                                                data-name="{{$booking['user']['name']}}"
                                                data-email="{{$booking['user']['email']}}"
                                                data-phone="{{$booking['user']['phone_number']}}"
                                                data-bookingat="{{formatDate($booking->created_at, 'H:i d/m/Y')}}"
                                                >Details</button>
                                 <form class="d-inline" action="{{route('admin_share_leads')}}" method="POST">
                                    @csrf
                                    @php $places = unserialize($booking->place_id) @endphp
                                        @foreach($places as $place)
                                            <input type="hidden" name="place_id" value="{{$place}}">
                                            @break
                                        @endforeach
                                    <input type="hidden" name="id" value="{{$booking->id}}">
                                    <input type="hidden" name="booking_id" value="{{$booking->booking_id}}">
                                    <button class="btn btn-secondary">Edit</button>
                                </form>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
    @include('admin.booking.modal_shared_detail')
@stop

@push('scripts')
    <script src="{{asset('admin/js/page_booking.js')}}"></script>
@endpush