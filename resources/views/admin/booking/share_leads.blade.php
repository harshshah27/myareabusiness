<!DOCTYPE html>
<html lang="en">
<head>
    @include('admin.layouts.head')
</head>

<body class="nav-md">
<div class="container body">
    <div class="main_container">
        <div class="col-md-3 left_col">
            @include('admin.layouts.sidebar_menu')
        </div>

        <!-- top navigation -->
        <div class="top_nav">
            @include('admin.layouts.top_nav')
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
@section('main')
<div class="page-title">
        <div class="title_left">
            <h3>Share Leads</h3>
        </div>
    </div>
    <div class="clearfix"></div>

    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <div class="x_content">
                	<form id="share_lead" enctype="multipart/form" action="{{route('admin_booking_admin')}}" method="post">
                		<!-- ,['slug'=>$place->name,'id'=>$booking->id] -->
                		@csrf
                    <table class="table table-striped table-bordered golo-datatable">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Area</th>
                            <th>Category</th>
                            <th>Sub Category</th>
                        </tr>
                        </thead>
                        <tbody>
                        	<tr>
                        		<td class="checkbox-td text-center">
	                        		<input type="hidden" id="booking_id" name="booking_id" value="{{$booking->id}}">
	                        		<input type="hidden" id="" name="user_name" value="{{$booking->name}}">
	                        		<input type="hidden" id="" name="user_email" value="{{$booking->email}}">
	                        		<input type="hidden" id="" name="user_phone_number" value="{{$booking->phone_number}}">
	                        		<input type="hidden" id="" name="user_no_of_childers" value="{{$booking->numbber_of_children}}">
	                        		<input type="hidden" id="" name="user_no_of_adults" value="{{$booking->numbber_of_adult}}">
	                        		<input type="hidden" id="" name="user_message" value="{{$booking->message}}">
	                        		<input type="hidden" id="" name="user_booking_at" value="{{formatDate($booking->created_at, 'H:i d-m-Y')}}">
	                        		<input type="hidden" id="" name="user_booking_type" value="{{$booking->type}}">
                        			<input type="checkbox" class="checkbox_test" name="case[]" >
                        			<input type="hidden" id="" class="place_name_booking"  value="{{$place->name}}">
									<input type="hidden" id="" class="place_email_booking"  value="{{$place->email}}">
                        			<input type="hidden" class="place_id" id="place_id"  value="{{$place->id}}">
                        		</td>
                        		
                        		<td>{{$place->id}}</td>
                        		<!-- <input type="hidden" id="data" name="data" value="{{$booking}}"> -->
                        		<td>{{$place->name}}</td>
                        		<td>{{$place->city->name}}</td>
                        		@foreach($categories as $category)
                        			<td>{{$category->name}}</td>
                        		@endforeach
                        		<td>
                        		@foreach($place_types as $type)
       								{{$type->name}}
                                @endforeach
                        		</td>
                        	</tr>
                        	@foreach($similar_places as $p => $place)
                        	<tr>
                        		
                        		<td class="checkbox-td text-center">
                        			<input type="checkbox" class="checkbox_test" name="case[]" >
                        			<input type="hidden" class="place_name_booking" id=""  value="{{$place->name}}">
                        			<input type="hidden" class="place_email_booking" id=""  value="{{$place->email}}">
                        			<input type="hidden" class="place_id" id=""  value="{{$place->id}}">
                        		</td>
                        		<td>{{$place->id}}</td>
                                @foreach($leads as $lead)
                        		<td class="text-success">{{$place->name}}</td>
                        		<td>{{$place->city->name}}</td>
                        		@foreach($categories as $category)
                        		<td>{{$category->name}}</td>
                        		@endforeach
                        		<td>
                        		@foreach($place->place_types as $sub_cat)
                        		{{$sub_cat->name}}
                        		@endforeach
                        			</td>
                        	</tr>
                        	@endforeach
                        </tbody>
                    </table>
                    <div class="d-flex justify-content-center">
                    	<button type="submit"  class="btn btn-primary" id="shareLeadBtn"  >Share Lead</button>
                    	<button type="button"  class="btn btn-primary " id="checkbtn"  >Check</button>
                    </div>
                    	</form>
                    					<div class="alert alert-success alert_booking booking_success d-none">
                                            <p>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
                                                    <path fill="#20D706" fill-rule="nonzero" d="M9.967 0C4.462 0 0 4.463 0 9.967c0 5.505 4.462 9.967 9.967 9.967 5.505 0 9.967-4.462 9.967-9.967C19.934 4.463 15.472 0 9.967 0zm0 18.065a8.098 8.098 0 1 1 0-16.196 8.098 8.098 0 0 1 8.098 8.098 8.098 8.098 0 0 1-8.098 8.098zm3.917-12.338a.868.868 0 0 0-1.208.337l-3.342 6.003-1.862-2.266c-.337-.388-.784-.589-1.207-.336-.424.253-.6.863-.325 1.255l2.59 3.152c.194.252.415.403.646.446l.002.003.024.002c.052.008.835.152 1.172-.45l3.836-6.891a.939.939 0 0 0-.326-1.255z"></path>
                                                </svg>
                                                {{__('You successfully created your booking.')}}
                                            </p>
                                        </div>
                                        <div class="alert alert-error alert_booking booking_error d-none">
                                            <p>
                                                <svg xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
                                                    <path fill="#FF2D55" fill-rule="nonzero"
                                                          d="M11.732 9.96l1.762-1.762a.622.622 0 0 0 0-.88l-.881-.882a.623.623 0 0 0-.881 0L9.97 8.198l-1.761-1.76a.624.624 0 0 0-.883-.002l-.88.881a.622.622 0 0 0 0 .882l1.762 1.76-1.758 1.759a.622.622 0 0 0 0 .88l.88.882a.623.623 0 0 0 .882 0l1.757-1.758 1.77 1.771a.623.623 0 0 0 .883 0l.88-.88a.624.624 0 0 0 0-.882l-1.77-1.771zM9.967 0C4.462 0 0 4.462 0 9.967c0 5.505 4.462 9.967 9.967 9.967 5.505 0 9.967-4.462 9.967-9.967C19.934 4.463 15.472 0 9.967 0zm0 18.065a8.098 8.098 0 1 1 8.098-8.098 8.098 8.098 0 0 1-8.098 8.098z"></path>
                                                </svg>
                                                {{__('An error occurred. Please try again.')}}
                                            </p>
                                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    </div>
        <!-- /page content -->

        <!-- footer content -->
        <footer>
            <div class="pull-right">
                <!-- lara.getgolo.com - version {{config('app.version')}} -->
            </div>
            <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
    </div>
</div>
<script src="{{asset('/admin/vendors/jquery/dist/jquery.min.js')}}"></script>
<script type="text/javascript">
	$(document).ready(function(){
		$('#shareLeadBtn').hide();
		$('#shareLeadBtn').click(function(){
			$(this).html(`<i class="fa fa-spinner fa-spin"></i>`).text('Sending...');
		});
		$('#checkbtn').click(function(){
			var checkedNum = $('input[name="case[]"]:checked').length;
			if (!checkedNum) {
				// $('#shareLeadBtn').prop('disabled', true);
				alert('Please select atleast one Place');
			}
			if(checkedNum){
						// $('#shareLeadBtn').attr('type','submit');

				var booking_id = $('#booking_id').val();
				var place = [];
				$("input[name^='place_id']").each(function () {
					place.push($(this).val());
 			  		console.log(place);
				});
				var place_name = [];
				$("input[name^='place_name']").each(function () {
					place_name.push($(this).val());
 			  		console.log(place_name);
				});
			// return ;// var place_id = $('#place_id').val();
			$.ajax({
                type: 'POST',
                url: '{{url("admincp/check_leads")}}',
                data: {
                	"_token": "{{ csrf_token() }}",
                	place_id: place,
                	booking_id : booking_id,
                	place_name : place_name
                },
                success: function(data){
                   if(data && data !== '0'){
                   	// data = data.responseText;
                   	console.log(data);
                       alert( 'The Lead is already shared to : ' +data);
                   } else if (data == '0'){
        				// $('#shareLeadBtn').html(`<i class="fa fa-spinner fa-spin"></i>`).prop('disabled', true);
        				$('#checkbtn').hide();
        				alert('You can share lead to this place...');
						$('#shareLeadBtn').show();
                } }
            });

			}
		});
			
		$("input[name='case[]']:checkbox").on('click',function(e){
			if($(this).prop("checked") == true){
				$(this).parent('.checkbox-td').find('.place_name_booking').attr('name','place_name[]');
				$(this).parent('.checkbox-td').find('.place_email_booking').attr('name','place_email[]');
				$(this).parent('.checkbox-td').find('.place_id').attr('name','place_id[]');
        				$('#checkbtn').show();
        				$('#shareLeadBtn').hide();
				// $('#checkbtn').prop('disabled', false);
				// $('#')
            }
            else if($(this).prop("checked") == false){
                $(this).parent('.checkbox-td').find('.place_name_booking').removeAttr('name','place_name[]');
                $(this).parent('.checkbox-td').find('.place_email_booking').removeAttr('name','place_email[]');
                $(this).parent('.checkbox-td').find('.place_id').removeAttr('name','place_id[]');
        				$('#checkbtn').show();
        				$('#shareLeadBtn').hide();
				// $('#checkbtn').prop('disabled', true);
            }
		});
	})
</script>
@include('admin.layouts.footer')
</body>
</html>